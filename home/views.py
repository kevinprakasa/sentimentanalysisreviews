from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed
from new_sentiment import NaiveBayesClassifier
import json
# Create your views here.

cla = NaiveBayesClassifier()

def index(request):
    accuracy = cla.accuracy
    precision_recall_fsupport = cla.precision_recall_f_score
    confusion_matrix = cla.confusion_matrix
    most_relevant_words = cla.mostRelevantWords()
    total_positive_review_train = (cla.positive_sentences)
    total_negative_review_train = (cla.negative_sentences)
    total_neutral_review_train = (cla.neutral_sentences)
    total_positive_review_test = cla.total_positive_reviews_test
    total_negative_review_test = cla.total_negative_reviews_test
    total_neutral_review_test = cla.total_neutral_reviews_test
    total_positive = total_positive_review_test+total_positive_review_train
    total_negative = total_negative_review_test+total_negative_review_train
    total_neutral = total_neutral_review_test+total_neutral_review_train
    return render(request, 'home.html', {
        'accuracy': accuracy,
        'precision_recall_fsupport': precision_recall_fsupport,
        'confusion_matrix': confusion_matrix,
        'most_relevant_words':  most_relevant_words,
        'total_positive_review_train': total_positive_review_train,
        'total_negative_review_train': total_negative_review_train,
        'total_neutral_review_train': total_neutral_review_train,
        'total_positive_review_test': total_positive_review_test,
        'total_negative_review_test': total_negative_review_test,
        'total_neutral_review_test': total_neutral_review_test,
        'total_positive': total_positive,
        'total_neutral': total_neutral,
        'total_negative': total_negative,
    })

def postClassifyText(request):
    if (request.method == "POST"):
        result = cla.classify(request.POST['text'])
        return JsonResponse({
            "text_class":result[0], 
            "positive_prediction": result[1],
            "neutral_prediction": result[2],
            "negative_prediction": result[3]
        })
    else:
        return HttpResponseNotAllowed(['POST'])