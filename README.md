Information Retrieval - Final Project

Kelompok: IRetrieval
Sentiment Analysis Pantai Bali melalui Review TripAdvisors


Nama Anggota:
- Kevin Prakasa 1606917696
- William Rumanta 1606895461

If you want to run the program locally, please use the following steps:
1. Use python virtual environment (virtualenv)
2. Install dependencies using "pip3 install -r requirements.txt"
3. Download nltk stopwords by 
	a) run python shell (type python3 in terminal)
	b) type import nltk
	c) type nltk.download('stopwords')
4. Runserver django (python3 manage.py runserver)
5. access localhost:8000

The runserver process may take some time due to computing
the program by default. But once it successfully loads the html,
it will not require any longer time.

gitlab: https://gitlab.com/kevinprakasa/sentimentanalysisreviews
heroku: http://kutabeachsentiment.herokuapp.com
